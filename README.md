# ==================================================
# 00h35m_LYNDA_DevSecOps - Burning Questions
Q: What is secured and what are not?  
A:

![EvolutionOfInfrastructure](images/EvolutionOfInfrastructure.png)

Q: Is security relevant with serverless?  
A: Yes

![Lambhack](images/Lambhack.png)

Q: What is chaos engineering?  
A:

![DefinitionOfChaosEngineering](images/DefinitionOfChaosEngineering.png)

Example with Netflix:
https://netflix.github.io/chaosmonkey/

Q: Who are the main auditors?  
A:

![AuditorGroup](images/AuditorGroup.png)

Also, http://dearauditor.org/

Q: How do you start a DevSecOps career?  
A:

![StartADevSecOpsCareer](images/StartADevSecOpsCareer.png)

Q: What are the main phases/steps in DevOps?  
A:

![DevOpsPhases](images/DevOpsPhases.png)

Q: How does security win?  
A: Thanks to automation tools, security can be deploy as fast as development.

Q: How to make security part of the development process?  
A:

![ChangeThePerceptionOfSecurity](images/ChangeThePerceptionOfSecurity.png)

Q: What is the measure in DevSecOps?  
A:

![MeasureAcronym](images/MeasureAcronym.png)

![Measure_M](images/Measure_M.png)
![Measure_E](images/Measure_E.png)
![Measure_A](images/Measure_A.png)
![Measure_S](images/Measure_S.png)
![Measure_U](images/Measure_U.png)
![Measure_R](images/Measure_R.png)
![Measure_E_Last](images/Measure_E_Last.png)
# ==================================================
# 00h45m_LYNDA_DevSecOps - Continuous Application Security
Q: What are some of the important tools to know?  
A:
1. Sonarqube ---> For Static Security Testing (SST)
2. Sonarscanner
3. Zaproxy ---> For Dynamic Security Testing (DST)
4. Contrast CE ---> For Interactive Application Security Testing (IAST)
5. WebGoat
6. Anchore-engine ---> For Container Security Testing (CST)
7. Anchore CLI
8. OWASP Dependency Check ---> For Library Security Testing (LST)
9. TruffleHog ---> Secret Scanning
10. OWASP Glue (https://owasp.org/www-project-glue-tool/)
11. OWASP Defect Dojo (https://owasp.org/www-project-defectdojo/)

Q: What is DevSecOps?  
A:

![WhatIsDevSecOps](images/WhatIsDevSecOps.png)

Q: What are some of DevSecOps goals?  
A:

![GoalOfDevSecOps](images/GoalOfDevSecOps.png)

**NOTE** Empowering teams using automation. 

![WhatTriggerDevSecOps](images/WhatTriggerDevSecOps.png)

Q: What is the traditional AppSec?  
A:

![TraditionalAppSec](images/TraditionalAppSec.png)

Q: What are some key of DevSecOps?  
A:

![KeyOfDevSecOps](images/KeyOfDevSecOps.png)

Q: What are CI practices?  
A:

![CIPractices](images/CIPractices.png)

Q: What are CD practices?  
A:

![CDPractices](images/CDPractices.png)

Q: Explain the Jenkins workflow for security?  
A:

![JenkinsWorkflowWithSecurityTools](images/JenkinsWorkflowWithSecurityTools.png)

![ResultWorkflowForSecurity](images/ResultWorkflowForSecurity.png)
# ==================================================
# 00h53m_LYNDA_DevSecOps Tips for Success 2020
Q: Software methodology?  
A:
* Waterfall
![WaterfallModel](images/WaterfallModel.png)
  
* Agile
![AgileModel](images/AgileModel.png)

Q: Why security work is unfair?  
A:

![WhySecurityWorkIsUnfair](images/WhySecurityWorkIsUnfair.png)

Q: Beside OWASP, what other organization is good to know?  
A: ISC2 (https://www.isc2.org/)

Q: Learn from security experts?  
A:

![LearnFromSecurityExpert](images/LearnFromSecurityExpert.png)

Q: What is the principle of chaos?  
A:

![PrincipleOfChaos](images/PrincipleOfChaos.png)

Q: What is chaos experiments?  
A:

![ChaosExperiments01](images/ChaosExperiments01.png)

![ChaosExperiments02](images/ChaosExperiments02.png)

Q: What is security chaos engineering?  
A:

![SecurityChaosEngineering](images/SecurityChaosEngineering.png)

Q: Where does continuous verification fits?  
A:

![ContinuousVerification](images/ContinuousVerification.png)

Q: Culture?  
A:

![SecurityInCompanyCulture](images/SecurityInCompanyCulture.png)

Q: Who are the auditors?  
A:

![WhoAreTheAuditor](images/WhoAreTheAuditor.png)

Q: What is the rugged manifesto?  
A: see: https://ruggedsoftware.org/

Q: What is the software bill of material?  
A: See: https://cybersecurity.att.com/blogs/security-essentials/software-bill-of-materials-sbom-does-it-work-for-devsecops

Q: What is CAMS?  
A:

![CAMS](images/CAMS.png)
# ==================================================
# 00h54m_LYNDA_DevOps Foundations - DevSecOps
Q: Why waterfall model is dead?  
A:

![WaterfallIsDead](images/WaterfallIsDead.png)

Q: What does agile model solves?  
A:

![AgileBenefits](images/AgileBenefits.png)

Q: What does DevOps bring to agile?  
A:

![DevOpsBenefits](images/DevOpsBenefits.png)

![DevOpsCultureChange](images/DevOpsCultureChange.png)

Q: What is the problem with the security scan workflow?  
A:

![NormalSecurityScan](images/NormalSecurityScan.png)

Q: DevSecOps versus Rugged manifesto?  
A:

![DevSecOpsVsRugged](images/DevSecOpsVsRugged.png)

Q: What do we move to the left?  
A:

![MoveToTheLeft](images/MoveToTheLeft.png)

Q: Why do company move to the cloud?  
A:

![CloudGoals](images/CloudGoals.png)

Q: What are the main category of security tools/softwares?  
A:
1. Static Application Security Testing (SAST)
   * Commercial: Fortify, AppScan, CheckMarx
   * Open Source: FindSecBugs, Brakeman, PMD
   
![SAST](images/SAST.png)

2. Dynamic Application Security Testing (DAST)
   * Commercial: WebInspect, Burp, AppSpider
   * Open Source: ZAP
   
![DAST](images/DAST.png)

3. Interactive Application Security Testing (IAST)
   * Commercial: Contrast, Seeker

![IAST](images/IAST.png)

Q: CI vs CD?  
A:

![CIGoals](images/CIGoals.png)

![CDGoals](images/CDGoals.png)

Q: Example of automation process?  
A:

![LeverageExistingProcess](images/LeverageExistingProcess.png)

Q: App vs Process?  
A:

![DifferentAppDifferentProcess](images/DifferentAppDifferentProcess.png)

Q: Examples of useful tools?  
A:
1. FindSecurityBugs
2. OWASP Zap
3. Sqlmap
4. OpenVAS
5. Recon-Ng
6. OWASP Glue
# ==================================================
# 01h12m_LYNDA_DevSecOps - Building a Secure Continuous Delivery Pipeline
Q: What is the order of magnitude staffing problem?  
A:

![OrderOfMagnitudeStaffingProblem](images/OrderOfMagnitudeStaffingProblem.png)

Q: What are the 5 stages of CI/CD pipeline?  
A:
1. Develop = Where the design and the development of the application happens.
2. Inherit = Where software dependencies get bundle into our application.
3. Build = Where the pipeline run the build step and the unit testing and/or acceptance testing.
4. Deploy = Move the application to the user.
5. Operate = When this application is up and running and the users consume the application.

![CICDPipeline](images/CICDPipeline.png)

Q: What are the goals of a DevSecOps Toolchain?  
A:
1. The ability to change and meet demands
2. 

Q: What are the main components of a secure development process?  
A:
1. Threat modeling:
   Use the STRIDE acronym:
   * Spoofing of user identity
   * Tampering
   * Repudiation
   * Information disclosure
   * Denial of service
   * Elevation of privilege
   sources:
     * OWASP App Threat Modeling Cheat Sheet
     * OWASP Application Security Verification Standard
     * Mozilla Rapid Risk Assessment
2. Development standards
   Enforce linting and formatting using:
   * git-secret
   * git-hound
3. Static code analysis

![OpenSourceSAST](images/OpenSourceSAST.png)

![CommecialSAST](images/CommecialSAST.png)

Software Composition Analysis (SCA):
1. OWASP Dependency Check
2. Retire.js
3. Bundler-audit (Ruby)
4. SensioLabs Security Checker (Php)
5. Sonatype
6. Black Duck
7. Veracode
8. WhiteSource
9. Clair (Docker)
10. AquaSec (Docker)
11. TwistLock (Docker)

Dynamic Application Security Testing (DAST):
### General-Purpose Scanner
1. Arachni
2. Nikto
3. ZAP
4. Burp Suite
### Sqli Scanner
5. Sqlmap
### SSL/TLS Scanners
6. SSLScan
7. SSLyze
8. Gauntlt

### Auditing
1. Rundeck

Q: What are DevSecOps Instrumentation?  
A:
1. Metrics based
2. Provide API
3. Promotes learning
4. Attacker driven

Q: Give some example of bug bounty program?  
A:
1. BugCrowd
2. HackerOne

### Runtime Application Self-Protection (RASP) / Next-Gen Web Application Firewall
1. ModSecurity
2. ELK
3. StatsD
4. Contrast
5. Prevoty
6. Signal Sciences
7. tCell
# ==================================================
# 01h35m_LYNDA_DevSecOps - Automated Security Testing
Q: Some important standard in security?
1. PCIDSS (https://cybersecurity.att.com/solutions/pci-dss-compliance)
2. Hipaa (https://www.thebalance.com/hipaa-law-and-medical-privacy-2645657)

Q: What is the penetration testing methodology?  
A:
1. Recon
2. Mapping
3. Discovery
4. Exploit

Q: What are security automation best practices?  
A:
1. Do not slow down the build
2. Do not block the build or the release
3. Integrate ChatOps
4. Contain Thyself
# ==================================================
# 02h39m_PLURALSIGHT_Performing DevSecOps Automated Security Testing
Q: What is automatic security testing?  
A:

![AutomaticSecurityTesting](images/AutomaticSecurityTesting.png)

Q :What do we mean by "shift left"?  
A:

![WhatIsShiftLeft](images/WhatIsShiftLeft.png)

## Code Tools
1. Linters
2. Secrets
   * TruffleHog
   * Pre-Commit
   * Detect-Secrets
3. Code Quality
   * Sonarqube

## Third-party Library Scanners Tools
1. OWASP Dependency Check

## Container Tools
1. Anchore Engine

## Infrastructure Tools
1. Nikto
2. OWASP ZAP
# ==================================================
# 03h32m_LinuxAcademy_DevSecOps Essentials
![DevSecOpsAutomation](images/DevSecOpsAutomation.png)

![DevSecOpsAutomationUseCases](images/DevSecOpsAutomationUseCases.png)

![MeasuringDevSecOpsSuccess](images/MeasuringDevSecOpsSuccess.png)

![OverviewOfDevSecOps](images/OverviewOfDevSecOps.png)
# ==================================================
Useful websites:
* https://www.sans.org/
* https://honeypy.readthedocs.io/en/latest/
* https://opencredo.com/blogs/what-is-continuous-verification/
